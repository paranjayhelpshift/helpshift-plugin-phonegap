package com.helpshift.android;

import android.util.Log;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.Map;
import java.util.List;
import java.util.HashMap;

import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import com.helpshift.*;
public class HSJSONUtils {

  private static final String TAG = "PhoneGap/HelpShiftDebug";

  public static HashMap convertToHashMap(JSONObject argumentsObject) {
    HashMap<String,Object> config = new HashMap<String,Object>();
    try {
      Object value = argumentsObject.get("enableContactUs");
      if(value != null) {
        config.put("enableContactUs", stringToEnum(value.toString()));
      }
      config.put("enableInAppNotification", convertKey("enableInAppNotification",argumentsObject));
      config.put("enableDialogUIForTablets", convertKey("enableDialogUIForTablets",argumentsObject));
      config.put("requireEmail", convertKey("requireEmail",argumentsObject));
      config.put("hideNameAndEmail", convertKey("hideNameAndEmail",argumentsObject));
      config.put("enableFullPrivacy", convertKey("enableFullPrivacy",argumentsObject));
      config.put("showSearchOnNewConversation", convertKey("showSearchOnNewConversation",argumentsObject));
      config.put("gotoConversationAfterContactUs", convertKey("gotoConversationAfterContactUs",argumentsObject));
      JSONObject metaData = argumentsObject.getJSONObject("HSCUSTOMMETADATA");
      HashMap metaMap = (HashMap) HSJSONUtils.toMap(metaData);
      if(metaData.has("HSTAGSKEY")) {
        List tags = HSJSONUtils.toList(metaData.getJSONArray("HSTAGSKEY"));
        String[] tagsArray = new String[tags.size()];
        metaMap.put(Helpshift.HSTagsKey, (String[])tags.toArray(tagsArray));
      }
      config.put(Helpshift.HSCustomMetadataKey,metaMap);
    } catch (Exception e) {
      Log.e(TAG,e.toString());
    }
    return config;
  }

  private static boolean convertKey(String key,JSONObject argumentsObject) {
    try {
      Object value = argumentsObject.get(key);
      if (value == null) {
        return false;
      } else if(value.toString().equals("YES")) {
        return true;
      } else if(value.toString().equals("NO")){
        return false;
      }
    } catch (Exception e) {
      return false;
    }
    return false;
  }

  private static Helpshift.ENABLE_CONTACT_US stringToEnum(String str) {
    Helpshift.ENABLE_CONTACT_US returnval = null;
    if(str.equals("ALWAYS")) {
      returnval = Helpshift.ENABLE_CONTACT_US.ALWAYS;
    } else if(str.equals("AFTER_VIEWING_FAQS")) {
      returnval = Helpshift.ENABLE_CONTACT_US.AFTER_VIEWING_FAQS;
    } else if(str.equals("NEVER")) {
      returnval = Helpshift.ENABLE_CONTACT_US.NEVER;
    }
    return returnval;
  }
  public static String[] getJSONObjectKeys(JSONObject inputObject) {
    Iterator keys = inputObject.keys();
    ArrayList<String> objectKeys = new ArrayList<String>();

    while (keys != null && keys.hasNext()) {
      objectKeys.add((String)keys.next());
    }
    String[] returnArray = new String[objectKeys.size()];
    return objectKeys.toArray(returnArray);
  }

  public static HashMap<String, String> toStringHashMap (JSONObject object) {
    HashMap<String, String> map = new HashMap<String, String>();
    Iterator keys = object.keys();
    while (keys.hasNext()) {
      String key = (String) keys.next();
      try {
        if (object.get(key) instanceof String) {
          map.put(key, object.getString(key));
        }
      } catch (JSONException e) {
        Log.d(TAG, "JsonException ", e);
      }
    }
    return map;
  }

  // Taken from gist here : https://gist.github.com/codebutler/2339666
  public static Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap();
        Iterator keys = object.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            map.put(key, fromJson(object.get(key)));
        }
        return map;
    }

    public static List toList(JSONArray array) throws JSONException {
        List list = new ArrayList();
        for (int i = 0; i < array.length(); i++) {
            list.add(fromJson(array.get(i)));
        }
        return list;
    }

    private static Object fromJson(Object json) throws JSONException {
        if (json == JSONObject.NULL) {
            return null;
        } else if (json instanceof JSONObject) {
            return toMap((JSONObject) json);
        } else if (json instanceof JSONArray) {
            return toList((JSONArray) json);
        } else {
            return json;
        }
    }
}
