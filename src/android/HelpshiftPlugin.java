
package com.helpshift.android;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.util.Log;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Iterator;

import com.helpshift.*;


public class HelpshiftPlugin extends CordovaPlugin {

    private Context cordovaActivity;
    private static final String TAG = "PhoneGap/HelpShiftDebug";
    // Helpshift methods
    private static final String INSTALL="install";

    private static final String SHOW_CONVERSATION="showConversation";
    private static final String SHOW_FAQS="showFAQs";
    private static final String SHOW_SINGLE_FAQ="showSingleFAQ";
    private static final String SHOW_FAQ_SECTION="showFAQSection";
    private static final String SHOW_ALERT_TO_RATE_APP="showAlertToRateAppWithURL";

    private static final String SET_USER_IDENTIFIER="setUserIdentifier";
    private static final String SET_NAME_AND_EMAIL="setNameAndEmail";
    private static final String LEAVE_BREAD_CRUMB="leaveBreadCrumb";
    private static final String CLEAR_BREAD_CRUMBS="clearBreadCrumbs";

    private static final String GET_NOTIFICATION_COUNT="getNotificationCountFromRemote";
    private static final String REGISTER_DEVICE_TOKEN="registerDeviceToken";
    private static final String REGISTER_SESSION_DELEGATES="registerSessionDelegates";
    private static final String REGISTER_CONVERSATION_DELEGATES="registerConversationDelegates";
    private static final String HANDLE_PUSH="handlePush";
    private static final String LOGIN="login";
    private static final String LOGOUT="logout";
    private static final String SET_SDK_LANGUAGE="setSDKLanguage";

    private HelpshiftDelegate delegateObject;
    private CallbackContext notificationCallback;

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        delegateObject = new HelpshiftDelegate();
        Helpshift.setDelegate(delegateObject);
    }
    @Override
    public boolean execute(String function, JSONArray arguments, CallbackContext callbackContext) throws JSONException
    {


        // Initialize function
        if (INSTALL.equals(function)) {
            String apiKey = arguments.getString(0);
            String domainName = arguments.getString(1);
            String appID = arguments.getString(2);

            if (arguments.length() >= 4) {
                HashMap<String,Object> config = new HashMap<String,Object>();
                config = HSJSONUtils.convertToHashMap(arguments.getJSONObject(3));
                Helpshift.install (cordova.getActivity().getApplication(), apiKey, domainName, appID,config);
                callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
                return true;
            }
            Helpshift.install (cordova.getActivity().getApplication(), apiKey, domainName, appID);
            return true;
        } else if(SHOW_FAQS.equals(function)) {

            if (arguments.length() >= 1) {
                HashMap<String,Object> config = new HashMap<String,Object>();
                config = HSJSONUtils.convertToHashMap(arguments.getJSONObject(0));
                Helpshift.showFAQs(cordova.getActivity(),config);
                callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
                return true;
            }
            Helpshift.showFAQs(cordova.getActivity());
            callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
            return true;
        } else if(SHOW_CONVERSATION.equals(function)) {

            if (arguments.length() >= 1) {
                HashMap<String,Object> config = new HashMap<String,Object>();
                config = HSJSONUtils.convertToHashMap(arguments.getJSONObject(0));
                Helpshift.showConversation(cordova.getActivity(),config);
                callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
                return true;
            }
            Helpshift.showConversation(cordova.getActivity());
            callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
            return true;
        } else if(SHOW_SINGLE_FAQ.equals(function)) {
            String faqId = arguments.getString(0);
            if (arguments.length() >= 2) {
                HashMap<String,Object> config = new HashMap<String,Object>();
                config = HSJSONUtils.convertToHashMap(arguments.getJSONObject(0));
                Helpshift.showSingleFAQ(cordova.getActivity(),faqId,config);
                callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
                return true;
            }
            Helpshift.showSingleFAQ(cordova.getActivity(),faqId);
            callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
            return true;
        } else if(SHOW_FAQ_SECTION.equals(function)) {
            String sectionId = arguments.getString(0);
            if (arguments.length() >= 2) {
                HashMap<String,Object> config = new HashMap<String,Object>();
                config = HSJSONUtils.convertToHashMap(arguments.getJSONObject(0));
                Helpshift.showFAQSection(cordova.getActivity(),sectionId,config);
                callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
                return true;
            }
            Helpshift.showFAQSection(cordova.getActivity(),sectionId);
            callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
            return true;
        }else if (SHOW_ALERT_TO_RATE_APP.equals(function)) {
            PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT, "");
            result.setKeepCallback(true);
            this.notificationCallback = callbackContext;
            HSAlertToRateAppListener actionListener =  new HSAlertToRateAppListener() {
                public void onAction(Helpshift.HS_RATE_ALERT action) {
                String msg = "";
                switch (action) {
                    case CLOSE:
                    msg = "HS_RATE_ALERT_CLOSE";
                    break;
                    case FEEDBACK:
                    msg = "HS_RATE_ALERT_FEEDBACK";
                    break;
                    case SUCCESS:
                    msg = "HS_RATE_ALERT_SUCCESS";
                    break;
                    case FAIL:
                    msg = "HS_RATE_ALERT_FAIL";
                    break;
                }
                notificationCallback.success(msg);
            }
          };
          Helpshift.showAlertToRateApp(arguments.getString(0), actionListener);
          return true;
      } else if (SET_NAME_AND_EMAIL.equals(function)) {
        String name = null;
        String email = null;
        name = arguments.getString(0);
        email = arguments.getString(1);
        if(arguments.isNull(0)) {
            name = null;
        }
        if(arguments.isNull(1)) {
            email = null;
        }
        Helpshift.setNameAndEmail(name, email);
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (SET_USER_IDENTIFIER.equals(function)) {
        Helpshift.setUserIdentifier(arguments.getString(0));
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (REGISTER_DEVICE_TOKEN.equals(function)) {
        Helpshift.registerDeviceToken(cordova.getActivity(), arguments.getString(0));
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (LEAVE_BREAD_CRUMB.equals(function)) {
        Helpshift.leaveBreadCrumb(arguments.getString(0));
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (CLEAR_BREAD_CRUMBS.equals(function)) {
        Helpshift.clearBreadCrumbs();
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (LOGIN.equals(function)) {
        String identifier = arguments.getString(0);
        String name = arguments.getString(1);
        String email = arguments.getString(2);
        Helpshift.login(identifier,name,email);
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (LOGOUT.equals(function)) {
        Helpshift.logout();
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if (GET_NOTIFICATION_COUNT.equals(function)) {
        int notifCount = Helpshift.getNotificationCount();
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, notifCount));
        return true;
      } else if (SET_SDK_LANGUAGE.equals(function)) {
        String locale = arguments.getString(0);
        Helpshift.setSDKLanguage(locale);
        callbackContext.sendPluginResult( new PluginResult(PluginResult.Status.OK, ""));
        return true;
      } else if(REGISTER_SESSION_DELEGATES.equals(function)) {
            PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT, "");
            result.setKeepCallback(true);
            HelpshiftDelegate.delegateSessionCB  = callbackContext;
            return true;
      } else if(REGISTER_CONVERSATION_DELEGATES.equals(function)) {
            PluginResult result = new PluginResult(PluginResult.Status.NO_RESULT, "");
            result.setKeepCallback(true);
            HelpshiftDelegate.delegateConversationCB  = callbackContext;
            return true;
      }
        return false;
    }
}