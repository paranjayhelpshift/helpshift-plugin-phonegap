package com.helpshift.android;

import com.helpshift.*;
import java.io.File;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;

class HelpshiftDelegate implements Helpshift.HelpshiftDelegate {

  public static  CallbackContext delegateSessionCB;
  public static  CallbackContext delegateConversationCB;
  @Override
  public void helpshiftSessionBegan() {
    if(delegateSessionCB != null)
    {
      JSONArray array = new JSONArray();
      array.put("eventname :");
      array.put("Session_Start");
      delegateSessionCB.success(array);
    }
  }

  @Override
  public void helpshiftSessionEnded() {
    if(delegateSessionCB != null)
    {
      JSONArray array = new JSONArray();
      array.put("eventname :");
      array.put("Session_End");
      delegateSessionCB.success(array);
    }
  }

  @Override
  public void newConversationStarted(String newConversationMessage) {
    if(delegateConversationCB != null)
    {
      JSONArray array = new JSONArray();
      array.put("eventname :");
      array.put("newConversationStarted");
      array.put("newConversationMessage :");
      array.put(newConversationMessage);
      delegateConversationCB.success(array);
    }
  }

  @Override
  public void userRepliedToConversation(String newMessage) {
    if(delegateConversationCB != null)
    {
      JSONArray array = new JSONArray();
      array.put("eventname :");
      array.put("userRepliedToConversation");
      array.put("newMessage :");
      array.put(newMessage);
      delegateConversationCB.success(array);
    }
  }

  @Override
  public void userCompletedCustomerSatisfactionSurvey(int rating, String feedback) {
    if(delegateConversationCB != null)
    {
      JSONArray array = new JSONArray();
      array.put("eventname :");
      array.put("userCompletedCustomerSatisfactionSurvey");
      array.put("rating :");
      array.put(rating);
      array.put("feedback :");
      array.put(feedback);
      delegateConversationCB.success(array);
    }
  }

  @Override
  public void displayAttachmentFile(File attachmentFile) {
    //NOP
  }

  @Override
  public void didReceiveNotification(int newMessagesCount) {
    if(delegateConversationCB != null)
    {
      JSONArray array = new JSONArray();
      array.put("eventname :");
      array.put("didReceiveNotification");
      array.put("newMessagesCount :");
      array.put(newMessagesCount);
      delegateConversationCB.success(array);
    }
  }
}