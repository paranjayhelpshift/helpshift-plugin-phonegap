
#import <Cordova/CDV.h>
#import "Helpshift.h"

@interface HelpshiftPlugin:CDVPlugin <HelpshiftDelegate>

- (NSMutableDictionary*) convertMetaData : (NSMutableDictionary*) config;

- (void)install :(CDVInvokedUrlCommand*)command;

- (void) showFAQs:(CDVInvokedUrlCommand*)command;

- (void) showConversation:(CDVInvokedUrlCommand*)command;

- (void) showFAQSection:(CDVInvokedUrlCommand*)command;

- (void) showSingleFAQ:(CDVInvokedUrlCommand*)command;

- (void) setUserIdentifier:(CDVInvokedUrlCommand *)command;

- (void) setNameAndEmail:(CDVInvokedUrlCommand*)command;

- (void) leaveBreadCrumb:(CDVInvokedUrlCommand*)command;

- (void) clearBreadCrumbs:(CDVInvokedUrlCommand*)command;

- (void) login :(CDVInvokedUrlCommand*)command;

- (void) logout :(CDVInvokedUrlCommand*)command;

- (void) getNotificationCountFromRemote:(CDVInvokedUrlCommand*)command;

- (void) setSDKLanguage:(CDVInvokedUrlCommand*)command;

- (void) showAlertToRateAppWithURL: (CDVInvokedUrlCommand*) command;
@end