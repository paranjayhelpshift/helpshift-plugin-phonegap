
var HelpshiftPlugin = {

    HS_CUSTOM_METADATA_KEY: "hs-custom-metadata",

    HS_TAGS_KEY: "hs-tags",

    HS_RATE_ALERT_CLOSE: "HS_RATE_ALERT_CLOSE",

    HS_RATE_ALERT_FEEDBACK: "HS_RATE_ALERT_FEEDBACK",

    HS_RATE_ALERT_SUCCESS: "HS_RATE_ALERT_SUCCESS",

    HS_RATE_ALERT_FAIL: "HS_RATE_ALERT_FAIL",

    newConversationStartedCB: null,
    userRepliedToConversationCB: null,
    userCompletedCustomerSatisfactionSurveyCB: null,
    didReceiveInAppNotificationWithMessageCountCB: null,
    didReceiveNotificationCB: null,

    sessionStartCB: null,
    sessionEndCB: null,
    alertToRateAppCB: null,


    /**
     * Initialize Helpshift support. When initializing Helpshift you must pass these three params.
     * You initialize Helpshift by adding HelpshiftPG.init method in onDeviceReady method.
     *
     * @param {string} apiKey This is your developer API Key
     * @param {string} domainName This is your domain name without any http:// or forward slashes
     * @param {string} appId This is the unique ID assigned to your app
     * @param {object} [options] To initialise the helpshift SDK with additional configuration options
     *
     * @example HelpshiftPG.init("&lt;API_KEY&gt;", "&lt;DOMAIN_NAME&gt;", "&lt;APP_ID&gt;");
     */
    install: function (apiKey, domainName, appId, options) {
        if (options && typeof options === "object") {
            cordova.exec (null, null, "HelpshiftPlugin", "install", [apiKey, domainName, appId, options]);
        } else {
            cordova.exec (null, null, "HelpshiftPlugin", "install", [apiKey, domainName, appId]);
        }
    },

    showFAQs: function (options) {
        if (options && typeof options === "object") {
            console.log("SHOWFAQ");
            cordova.exec (null, null, "HelpshiftPlugin", "showFAQs", [options]);
        } else {
            cordova.exec (null, null, "HelpshiftPlugin", "showFAQs", []);
        }
    },

    showConversation: function (options) {
        if (options && typeof options === "object") {
            cordova.exec (null, null, "HelpshiftPlugin", "showConversation", [options]);
        } else {
            cordova.exec (null, null, "HelpshiftPlugin", "showConversation", []);
        }
    },

    /**
     * Show the helpshift screen with faqs from a particular section.
     * To show the Helpshift screen for showing a particular faq section
     * you need to pass the publish-id of the faq section.
     *
     * @param {string} faqSectionPublishId The publish id associated with the faq section which is shown
     *        in the FAQ page on the admin side (__yourcompanyname__.helpshift.com/admin/faq/).
     * @param {object} [options] To show FAQ Section with additional configuration options
     *
     * @example HelpshiftPG.showFAQSection ("&lt;PUBLISH_ID&gt;");
     *  HelpshiftPG.showFAQSection ("&lt;PUBLISH_ID&gt;", {
     *    "gotoConversationAfterContactUs": "NO",
     *    "enableContactUs": "ALWAYS",
     *    "presentFullScreenOniPad" : "YES"
     *  });
     */
    showFAQSection: function (faqSectionPublishId, options) {
        if (faqSectionPublishId && typeof faqSectionPublishId === "string") {
            if (options && typeof options === "object") {
                cordova.exec (null, null, "HelpshiftPlugin", "showFAQSection", [faqSectionPublishId, options]);
            } else {
                cordova.exec (null, null, "HelpshiftPlugin", "showFAQSection", [faqSectionPublishId]);
            }
        }
    },

    /**
     * Show the helpshift screen with a particular faq.
     * To show the Helpshift screen for showing a particular faq you need to pass the publish-id of the faq.
     *
     * @param {string} faqPublishId The publish id associated with the faq which is shown when you expand
     *        a single FAQ (__yourcompanyname__.helpshift.com/admin/faq/)
     * @param {object} [options] To show Single FAQ with additional configuration options
     *
     * @example HelpshiftPG.showSingleFAQ ("&lt;PUBLISH_ID&gt;");
     *  HelpshiftPG.showSingleFAQ ("&lt;PUBLISH_ID&gt;", {
     *    "gotoConversationAfterContactUs": "NO",
     *    "enableContactUs": "ALWAYS",
     *    "presentFullScreenOniPad" : "YES"
     *  });
     */
    showSingleFAQ: function (faqPublishId, options) {
        if (faqPublishId && typeof faqPublishId === "string") {
            if (options && typeof options === "object") {
                cordova.exec (null, null, "HelpshiftPlugin", "showSingleFAQ", [faqPublishId, options]);
            } else {
                cordova.exec (null, null, "HelpshiftPlugin", "showSingleFAQ", [faqPublishId]);
            }
        }
    },

    /**
     * Set the unique identifier for your users.
     * This is part of additional user configuration. You can setup the unique identifier
     * that this user will have with this api.
     *
     * @param {string} userIdentifier A unique string to identify your users.
     *
     * @example HelpshiftPG.setUserIdentifier ("user-id-100");
     */
    setUserIdentifier: function (userIdentifier) {
        if (userIdentifier && typeof userIdentifier === "string") {
            cordova.exec (null, null, "HelpshiftPlugin", "setUserIdentifier", [userIdentifier]);
        }
    },

    login: function (userIdentifier,name,email) {
        cordova.exec (null, null, "HelpshiftPlugin", "login", [userIdentifier,name,email]);
    },


    /**
     * Set the name and email of the application user.
     * This is part of additional user configuration. If this is provided through the api,
     * user will not be prompted to re-enter this information again.
     * If you wish to reset these values, please provide null for both params
     *
     * @param {string} name The name of the user.
     * @param {string} email The email of the user.
     *
     * @example HelpshiftPG.setNameAndEmail ("John", "john@example.com");
     */
    setNameAndEmail: function (name, email) {
        var lName = null, lEmail = null;
        if (name && typeof name === "string") {
            lName = name;
        }
        if (email && typeof email === "string") {
            lEmail = email;
        }
        cordova.exec (null, null, "HelpshiftPlugin", "setNameAndEmail", [lName, lEmail]);
    },

    /**
     * Add extra debug information regarding user-actions.
     * You can add additional debugging statements to your code, and see exactly what the user
     * was doing right before they reported the issue.
     *
     * @param {string} breadCrumb The string containing any relevant debugging information.
     *
     * @example HelpshiftPG.leaveBreadCrumb ("settings button");
     */
    leaveBreadCrumb: function (breadCrumb) {
        if (breadCrumb && typeof breadCrumb === "string") {
            cordova.exec (null, null, "HelpshiftPlugin", "leaveBreadCrumb", [breadCrumb]);
        }
    },

    logout: function () {
        cordova.exec (null, null, "HelpshiftPlugin", "logout", []);
    },

    getNotificationCountFromRemote: function () {
        cordova.exec (null, null, "HelpshiftPlugin", "getNotificationCountFromRemote", []);
    },

    setSDKLanguage: function (locale) {
        if (locale && typeof locale === "string") {
            cordova.exec (null, null, "HelpshiftPlugin", "setSDKLanguage", [locale]);
        }
    },

    /**
     * Clears Breadcrumbs list.
     * Breadcrumbs list stores upto 100 latest actions. You'll receive those in every Issue.
     * If for some reason you want to clear previous messages, you can use this method.
     *
     * @example HelshiftPG.clearBreadCrumbs ();
     */

    clearBreadCrumbs: function () {
        cordova.exec (null, null, "HelpshiftPlugin", "clearBreadCrumbs", []);
    },



    showAlertToRateAppWithURL: function (url,callBackFunction) {
        //var notifyCb = null;
        if (typeof callBackFunction === "function") {
            this.alertToRateAppCB = callBackFunction;
        }
        if (url && typeof url === "string") {
            cordova.exec (function (message) {
                if (this.alertToRateAppCB) {
                    this.alertToRateAppCB.apply (this, [message]);
                }
            }, null, "HelpshiftPlugin", "showAlertToRateAppWithURL", [url]);
        }
    },

    //For Android
    registerSessionDelegates: function (sessionStart,sessionEnd) {
        if (typeof sessionStart === "function" && typeof sessionEnd === "function") {
            this.sessionStartCB = sessionStart;
            this.sessionEndCB = sessionEnd;
            cordova.exec (function (message) {
                if(message.getJSONObject(0).getString("eventname") == "Session_Start") {
                    this.sessionStartCB.apply (this,[message]);
                } else if(message.getJSONObject(0).getString("eventname") == "Session_End") {
                    this.sessionEndCB.apply (this,[message]);
        }}, null, "HelpshiftPlugin", "registerSessionDelegates", []);
        }
    },

    //For ios
    registerSessionDelegatesIOS: function (sessionStart,sessionEnd) {
        if (typeof sessionStart === "function" && typeof sessionEnd === "function") {
            this.sessionStartCB = sessionStart;
            this.sessionEndCB = sessionEnd;
            console.log("Java script function registered !!!!!");
        }
    },

    //For Android
    registerConversationDelegates: function (newConversationStarted,userRepliedToConversation,userCompletedCustomerSatisfactionSurvey,didReceiveNotification) {
        if (typeof newConversationStarted === "function" && typeof userRepliedToConversation === "function" && typeof userCompletedCustomerSatisfactionSurvey === "function" && typeof didReceiveNotification === "function" ) {
            this.newConversationStartedCB = newConversationStarted;
            this.userRepliedToConversationCB =userRepliedToConversation;
            this.userCompletedCustomerSatisfactionSurveyCB = userCompletedCustomerSatisfactionSurvey;
            this.didReceiveNotificationCB = didReceiveNotification;
            cordova.exec (function (message) {
                if(message.getJSONObject(0).getString("eventname") == "newConversationStarted") {
                    this.newConversationStartedCB.apply (this,[message]);
                } else if(message.getJSONObject(0).getString("eventname") == "userRepliedToConversation") {
                    this.userRepliedToConversationCB.apply (this,[message]);
                } else if(message.getJSONObject(0).getString("eventname") == "userCompletedCustomerSatisfactionSurvey") {
                    this.userCompletedCustomerSatisfactionSurveyCB.apply (this,[message]);
                } else if(message.getJSONObject(0).getString("eventname") == "didReceiveNotification") {
                    this.didReceiveNotificationCB.apply (this,[message]);
                }
            }, null, "HelpshiftPlugin", "registerConversationDelegates", []);
        }
    },

    //For Ios
    registerConversationDelegatesIOS: function (newConversationStarted,userRepliedToConversation,userCompletedCustomerSatisfactionSurvey,didReceiveInAppNotificationWithMessageCount,didReceiveNotification) {
        if (typeof newConversationStarted === "function" && typeof userRepliedToConversation === "function" && typeof userCompletedCustomerSatisfactionSurvey === "function" && typeof didReceiveInAppNotificationWithMessageCount === "function" && typeof didReceiveNotification === "function" ) {
            this.newConversationStartedCB = newConversationStarted;
            this.userRepliedToConversationCB =userRepliedToConversation;
            this.userCompletedCustomerSatisfactionSurveyCB = userCompletedCustomerSatisfactionSurvey;
            this.didReceiveInAppNotificationWithMessageCountCB = didReceiveInAppNotificationWithMessageCount;
            this.didReceiveNotificationCB = didReceiveNotification;
        }
    },

    _processMetadata: function (options) {
        if (options && typeof options === "object") {
            if("HSCUSTOMMETADAKEY" in options && options["HSCUSTOMMETADAKEY"] instanceof Object) {
                var customMetadata = options["HSCUSTOMMETADAKEY"];
                if("HSTAGSKEY" in customMetadata && customMetadata["HSTAGSKEY"] instanceof Array) {
                    customMetadata["hs-tags"] = customMetadata["HSTAGSKEY"];
                }
                delete customMetadata ["HSTAGSKEY"];
                options["hs-custom-metadata"] = customMetadata;
            }
            delete options ["HSCUSTOMMETADAKEY"];
        }
        return options;
    },
    _nativeAppRateResponseCall: function (message) {
        if (this.alertToRateAppCB) {
            this.alertToRateAppCB.apply (null, [message]);
        }
    },

    _nativeNotificationCall: function (message) {
        if (this.didReceiveNotificationCB) {
            this.didReceiveNotificationCB.apply (null, [message]);
        }
    },
    _nativeInAppNotificationCall: function (message) {
        if (this.didReceiveInAppNotificationWithMessageCountCB) {
            this.didReceiveInAppNotificationWithMessageCountCB.apply (null, [message]);
        }
    },
    _nativeSessionBeganCall: function () {
        if (this.sessionStartCB) {
            this.sessionStartCB.apply (null, []);
        }
    },
    _nativeSessionEndedCall: function () {
        if (this.sessionEndCB) {
            this.sessionEndCB.apply (null, []);
        }
    },

    _nativeNewConversationStartedWithMessageCall: function (message) {
        if (this.newConversationStartedCB) {
            this.newConversationStartedCB.apply (null, [message]);
        }
    },
    _nativeUserRepliedToConversationWithMessageCall: function (message) {
        if (this.userRepliedToConversationCB) {
            this.userRepliedToConversationCB.apply (null, [message]);
        }
    },

    _nativeUserCompletedCustomerSatisfactionSurveyCall: function (message) {
        if (this.userCompletedCustomerSatisfactionSurveyCB) {
            this.userCompletedCustomerSatisfactionSurveyCB.apply (null, [message]);
        }
    }

};
module.exports = HelpshiftPlugin;
